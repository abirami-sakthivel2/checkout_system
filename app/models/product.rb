class Product < ApplicationRecord
  belongs_to :basket, optional: true

  enum vat_type: [:zero, :reduced, :standard]

  def vat_total
    #calculating VAT for the purchased items
    # if vat_rate is zero
    # then vat is 0% of product's price
    # if vat_rate is reduced
    # then vat is 5% of product's price
    # if vat_rate is standard
    # then vat is 20% of product's price

    if zero?
      if quantity == 2
        # every second Scotland Flag goes with £5 discount.
        self.price = self.price - (20 * 0.05)
        self.save
        self.price
      else
        self.price
      end
    elsif reduced?
      self.price * 0.05
    elsif standard?
      self.price * 0.2
    end
  end

  def total
    #adding VAT for the purchased items to product's price
    if zero?
      if quantity > 0
        (self.price)* quantity
      else
        self.price
      end
    else
      if quantity > 0
        (self.price + self.vat_total) * quantity
      else
        self.price + self.vat_total
      end

    end
  end

end
