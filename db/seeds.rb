# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.create(code: '01', name: 'Scotland Flag', price: 20)
Product.create(code: '02', name: "Children’s Car Seat", price: 33.90, vat_type: Product.vat_types[:reduced])
Product.create(code: '03', name: 'Magnetic Wrist Band', price: 9.00, vat_type: Product.vat_types[:standard])