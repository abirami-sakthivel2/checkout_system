class AddBasketToProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :products, :basket, index: true, foreign_key: true
  end
end
