Turbolinks.setProgressBarDelay(50);

function StartTurbolinksProgress() {
    if (!Turbolinks.supported) {
        return;
    }
    Turbolinks.controller.adapter.progressBar.setValue(0);
    Turbolinks.controller.adapter.progressBar.show();
}

function StopTurbolinksProgress() {
    if (!Turbolinks.supported) {
        return;
    }
    Turbolinks.controller.adapter.progressBar.hide();
    Turbolinks.controller.adapter.progressBar.setValue(100);
}

