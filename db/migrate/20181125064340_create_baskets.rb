class CreateBaskets < ActiveRecord::Migration[5.2]
  def change
    create_table :baskets do |t|
      t.float :total_price
      t.references :user, index: true, foreign_key: true


      t.timestamps
    end
  end
end
