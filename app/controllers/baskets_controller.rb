class BasketsController < ApplicationController

  def create
    @basket = Basket.first_or_initialize
    @basket.user = current_user

    product = Product.find(params[:product_id])
    product.update_attributes(quantity: product.quantity + 1)

    @basket.products << product
    @basket.save

    @basket.total_price = @basket.products.all.sum { |p| p.total }
    @basket.save

    #if customer spends more than £70 - he should get 12% off his purchase.
    if @basket.total_price > 70
      discounted_price = (@basket.total_price * 12) / 100
      @basket.total_price = @basket.total_price - discounted_price
      @basket.save
    end

    redirect_to products_path
  end

  def reset
    @basket = Basket.find(params[:basket_id])
    @basket.products.update_all(quantity: 0)
    @basket.products.zero.update_all(price: 20.0)
    @basket.update_attributes(product_ids: nil, total_price: nil)
    redirect_to products_path
  end
end