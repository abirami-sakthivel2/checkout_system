module ApplicationHelper
  def bootstrap_class_for flash_type
    {success: 'alert-success', error: 'alert-danger', alert: 'alert-warning', notice: 'alert-info'}[flash_type] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "text-center alert alert-dismissible fade show #{bootstrap_class_for(msg_type.to_sym)}") do
        concat content_tag(:button, 'x', class: 'close', data: {dismiss: 'alert'})
        concat simple_format(message)
      end)
    end
    nil
  end

  def active_class(path)
    return_class = ''
    if path.class == Array
      if path.any? { |str| request.path.include? str }
        return_class = 'active'
      end
    else
      if request.path.include?(path)
        return_class = 'active'
      end
    end
    return_class
  end

  def formatted_price(price)
    if price.blank?
      'N/A'
    else
      "€#{formatted_decimal(price)}"
    end
  end

  def formatted_decimal(number)
    if number.blank?
      ''
    else
      number_with_precision(number, precision: 2, delimiter: ',')
    end
  end

end
