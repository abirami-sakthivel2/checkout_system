class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :code, default: ""
      t.string :name, default: ""
      t.float :price
      t.integer :quantity, default: 0
      t.integer :vat_type, default: 0
      t.timestamps
    end
  end
end
